#pragma once
#include "MyGameEngine\Scene.h"

//テスト用クリアシーン
class ClearScene : public Scene
{
public:
	ClearScene();
	~ClearScene();

	void Init()   override;
	void Update() override;
	void Input()  override;
};