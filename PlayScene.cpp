#include "PlayScene.h"
#include "ClearScene.h"
#include "MyGameEngine\Camera.h"

PlayScene::PlayScene()
{
	_quad = nullptr;
	_angle = 0;
}


PlayScene::~PlayScene()
{

}

void PlayScene::Init()
{
	auto camera = Camera::Create();
	this->AddChild(camera);
	camera->Update();

	auto fbx = Fbx::Create("Assets\\face.fbx");
	this->AddChild(fbx);

	auto light = Light::Create();
	this->AddChild(light);
}

void PlayScene::Update()
{
	_angle++;
	//_quad->SetRotate(0, _angle, 0);
}

void PlayScene::Input()
{

}