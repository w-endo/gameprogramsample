#pragma once
#include "Global.h"

class Node
{
protected:
	Scene* _parent;

	D3DXVECTOR3 _size;        //サイズ
	D3DXVECTOR3 _anchorPoint; //アンカーポイント
	D3DXVECTOR3 _position;    //位置
	D3DXVECTOR3 _rotate;      //回転角度
	D3DXVECTOR3 _scale;       //拡大率

public:
	Node();
	virtual ~Node();
	virtual void Draw(){};

	//各セッター
	void SetAnchorPoint(float x, float y, float z = 0);
	void SetPosition(float x, float y, float z = 0);
	void SetRotate(float x, float y, float z);
	void SetRotate(float z);
	void SetScale(float x, float y, float z = 1);

	//各ゲッター
	D3DXVECTOR3 GetSize();
	D3DXVECTOR3 GetAnchorPoint();
	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotate();
	D3DXVECTOR3 GetScale();

	MyRect GetBoundingBox();
	void SetParent(Scene* parent);
	void RemoveFromParent();
};
