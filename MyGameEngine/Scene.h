#pragma once

#include "Global.h"
#include "Sprite.h"
#include "Label.h"
#include "Quad.h"
#include "Light.h"
#include "Fbx.h"
#include <vector>

class Scene
{
	std::vector<Node*> _nodes;
public:
	Scene();
	virtual ~Scene();
	void AddChild(Node* pNode);
	void Draw();
	virtual void Init()   = 0;
	virtual void Update() = 0;
	virtual void Input()  = 0;

	void RemoveChild(Node* pNode);
};