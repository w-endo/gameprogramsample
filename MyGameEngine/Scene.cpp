#include "Scene.h"


Scene::Scene()
{
}


Scene::~Scene()
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		SAFE_DELETE(_nodes[i]);
	}
}


void Scene::AddChild(Node* pNode)
{
	pNode->SetParent(this);
	_nodes.push_back(pNode);
}


void Scene::Draw()
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		_nodes[i]->Draw();
	}
}


void Scene::RemoveChild(Node* pNode)
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		if (pNode == _nodes[i])
		{
			SAFE_DELETE(_nodes[i]);
			_nodes.erase(_nodes.begin() + i);
			break;
		}
	}
}