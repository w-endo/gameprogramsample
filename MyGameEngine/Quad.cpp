#include "Quad.h"


Quad::Quad()
{
}


Quad::~Quad()
{
}

Quad* Quad::Create()
{
	auto quad = new Quad();
	quad->Load();
	return quad;
}

void Quad::Load()
{
	Vertex vertexList[] = {
		D3DXVECTOR3(-1, 1, 0), D3DXVECTOR3(0, 0, -1),
		D3DXVECTOR3(1, 1, 0), D3DXVECTOR3(0, 0, -1),
		D3DXVECTOR3(1, -1, 0), D3DXVECTOR3(0, 0, -1),
		D3DXVECTOR3(-1, -1, 0), D3DXVECTOR3(0, 0, -1),
	};

	g.pDevice->CreateVertexBuffer(sizeof(vertexList), 0, D3DFVF_XYZ | D3DFVF_NORMAL, D3DPOOL_MANAGED, &_vertexBuffer, 0);
	Vertex *vCopy;
	_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(vertexList));
	_vertexBuffer->Unlock();


	int indexList[] = { 0, 2, 3, 0, 1, 2 };
	g.pDevice->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32, D3DPOOL_MANAGED, &_indexBuffer, 0);
	DWORD *iCopy;
	_indexBuffer->Lock(0, 0, (void**)&iCopy, 0);
	memcpy(iCopy, indexList, sizeof(indexList));
	_indexBuffer->Unlock();

	ZeroMemory(&_material, sizeof(D3DMATERIAL9));
	_material.Diffuse.r = 1.0f;
	_material.Diffuse.g = 1.0f;
	_material.Diffuse.b = 1.0f;
}

void Quad::Draw()
{
	g.pDevice->SetStreamSource(0, _vertexBuffer, 0, sizeof(Vertex));
	g.pDevice->SetIndices(_indexBuffer);
	g.pDevice->SetMaterial(&_material);
	g.pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL);

	//�ړ��s��
	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, _position.x, _position.y, _position.z);

	//��]�s��
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(_rotate.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(_rotate.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(_rotate.z));

	//�g��k��
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, _scale.z);

	//���[���h�s��
	D3DXMATRIX world = scale * rotateZ * rotateX * rotateY * trans;


	g.pDevice->SetTransform(D3DTS_WORLD, &world);

	g.pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
}