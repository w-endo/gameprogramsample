#pragma once
#include "Node.h"

class Quad : public Node
{
	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
	};
	LPDIRECT3DVERTEXBUFFER9 _vertexBuffer;
	LPDIRECT3DINDEXBUFFER9	_indexBuffer;
	D3DMATERIAL9			_material;

public:
	Quad();
	~Quad();
	static Quad* Create();
	void Load();
	void Draw();
};