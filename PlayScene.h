#pragma once
#include "MyGameEngine\Scene.h"

class Quad;
class PlayScene : public Scene
{
	Quad* _quad;
	int   _angle;

public:
	PlayScene();
	~PlayScene();

	void Init()   override;
	void Update() override;
	void Input()  override;
};